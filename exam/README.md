# Practical Exam Code

Once you are satisty with your implementation on Google Collab, download your jupyter notebook and push your here for final evaluation.

Make sure rename your notebook with the following format:

- yourname_data_solution.ipynb
- yourname_training_solution.ipynb

In your notebook, please utilize _markdown_ to explain your action or result.