# Example Jupyter Notebook

Hosting coding example for JRP

## Image Classification

- [fashion mnist with tensorflow : style 1 without validation](https://gitlab.com/knabenphysik/jrp_note/-/blob/main/practical_code/classification_fashion_mnist.ipynb)
- [fashion mnist with tensorflow : style 2 with validation & tf.data method](https://gitlab.com/knabenphysik/jrp_note/-/blob/main/practical_code/classification_fashion_mnist_2.ipynb)
- [fashion mnist with tensorflow : style 3 with validation, confusion matrix](https://gitlab.com/knabenphysik/jrp_note/-/blob/main/practical_code/classification_fashion_mnist_3.ipynb)
- [fashion mnist with tensorflow : compare with other ML method](https://gitlab.com/knabenphysik/jrp_note/-/blob/main/practical_code/classification_fashion_mnist_4.ipynb)
- [tabular data classification : raisin classification](https://gitlab.com/knabenphysik/jrp_note/-/blob/main/practical_code/raisin_classification.ipynb)